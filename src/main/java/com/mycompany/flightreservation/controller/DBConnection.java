/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

import java.sql.ResultSet;

/**
 *
 * @author Room107
 */
public interface DBConnection {
     public ResultSet executeQuery(String query);
     public void update(String command);
     public void close();
        
}
