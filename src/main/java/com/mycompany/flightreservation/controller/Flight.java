/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

import com.mycompany.flightreservation.domain.AirportList;
import com.mycompany.flightreservation.domain.File;
import com.mycompany.flightreservation.domain.Trip;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Room107
 */
public class Flight  implements IFlightManagment{
    
    @Override
    public ArrayList<AirportList> searchFlight(Trip trip, boolean bestFare) throws MissingRequiredTripInfoException {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    ArrayList<AirportList> array = new ArrayList<>();
        DBConnecttionFactory dbfactory = new DBConnecttionFactory();
    File file = new File("mysql");
    
    
    DBConnection dbconnection = dbfactory.createDBConnection("mysql", file.getdomain_name(), file.getdb_name(), file.getuser_name(),file.getpassword(), file.getpors_name());
    //String brand,String host,String dbName,String user,String password,String port
    if(dbconnection != null){
        ResultSet set = dbconnection.executeQuery("select * from flight;");
    
        try {
            while(set.next()){
                array.add(new AirportList(set.getString(2), set.getString(3), set.getString(4), set.getString(5), set.getString(6), set.getString(7)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Flight.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Not Found");
        }
    }else{
        System.out.println("ERROR NOT FOUND");
    }
    return array;
    }
    
}
