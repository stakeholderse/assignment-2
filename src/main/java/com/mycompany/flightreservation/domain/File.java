/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain;

import static com.sun.org.glassfish.external.amx.AMXUtil.prop;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class File {
 private String brand;
    private String domain_name;
    private String db_name;
    private String db_table;
    private String pors_name;
    private String user_name;
    private String password;
    
   public File(String brand){
       if(brand.equalsIgnoreCase("mysql")){
       Properties prop = new Properties();
       InputStream input = null;
     try {
         input = new FileInputStream("src/main/resource/MySQL.properties");
        prop.load(input);
       this.brand=prop.getProperty("BRAND");
       domain_name=prop.getProperty("Domain_Name");
       db_name=prop.getProperty("DB_Name");
       db_table=prop.getProperty("DB_TABLE");
       pors_name=prop.getProperty("PORT_Number");
       user_name=prop.getProperty("User_Name");
       password=prop.getProperty("Password");
     } 
     catch (FileNotFoundException ex) {
         Logger.getLogger(File.class.getName()).log(Level.SEVERE, null, ex);
     } catch (IOException ex) {
         Logger.getLogger(File.class.getName()).log(Level.SEVERE, null, ex);
     }
       }
   } 
    
   public  String getbrand(){
       return brand;
   }
   public  String getdomain_name(){
       return domain_name;
   }
   public  String getdb_name(){
       return db_name;
   }
   public  String getdb_table(){
       return db_table;
   }
   public  String getpors_name(){
       return pors_name;
   }
   public  String getuser_name(){
       return user_name;
   }
   public  String getpassword(){
       return password;
   }
  
}
