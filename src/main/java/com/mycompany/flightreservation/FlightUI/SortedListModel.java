/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.FlightUI;

import java.util.SortedSet;
import javax.swing.AbstractListModel;

/**
 *
 * @author chanita
 */
class SortedListModel extends AbstractListModel {
    SortedSet<Object> model;
    public Object firstElement(){
      return model.first();
    }
    
    public Object lastElement(){
        return model.last();
    }
}
