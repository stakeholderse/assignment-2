/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.FlightUI;

/**
 *
 * @author chanita
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

public class MyFlightListCellRenderer extends JPanel
        implements ListCellRenderer<MyFlightDetail> {
    private ImageIcon icon ;
 

    public MyFlightListCellRenderer() {
        this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));         
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
    
    @Override
    public Component getListCellRendererComponent(JList list, MyFlightDetail value,
            int index, boolean isSelected, boolean cellHasFocus) {
        
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        this.removeAll();
        JLabel price = new JLabel("2055");
        JLabel airline = new JLabel("singapore");
        JLabel takeOff = new JLabel("Bkk 15.35");
        JLabel landing = new JLabel("CDG 05.30");
        JLabel stop = new JLabel("1(19h 55m)");
        JLabel image = new JLabel("");
        image.setIcon(new ImageIcon("src\\resource\\sg.png"));
        this.add(price);
        this.add(airline);
        this.add(takeOff);
        this.add(landing);
        this.add(stop);
        this.add(image);
        
        
        
        return this;

    }
    
}

